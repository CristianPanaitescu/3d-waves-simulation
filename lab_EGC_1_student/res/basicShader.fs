#version 330

varying vec3 normal0;
varying vec2 texCoord0;

uniform sampler2D sampler;


void main()
{
	gl_FragColor = texture2D(sampler, texCoord0);
}
