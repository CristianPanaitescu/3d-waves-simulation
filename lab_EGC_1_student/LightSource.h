#pragma once
#include "Object.h"
class LightSource :
	public Object
{
public:
	LightSource();
	~LightSource();

private:

	void			InitVertices(std::vector<Vertex>& vertices);
	void			InitIndices(std::vector<uint>& indices);
};

