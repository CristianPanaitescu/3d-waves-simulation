#version 330 compatibility
layout(triangles) in;
layout(triangle_strip, max_vertices= 6) out;
 
 varying vec2 texCoord0[3];
 in float light0[3];

 out float light1;

 out vec2 texCoords;

void main()
{	
	float step = 0.05;
for(int k = 0 ; k < 2 ; k++)
{
 for(int i=0; i<3; i++)
  {
	light1 = light0[i];
	texCoords = texCoord0[i];
    vec4 pos = gl_in[i].gl_Position;
    gl_Position = vec4(pos.x, pos.y - k * step, pos.z, pos.w);
    EmitVertex();
  }
  EndPrimitive();
}
 

}  