#version 330

in vec2 texCoords;

uniform sampler2D sampler;


in float light1;

void main()
{
	gl_FragColor = texture2D(sampler, texCoords) * vec4(light1, light1, light1, 1);
}
