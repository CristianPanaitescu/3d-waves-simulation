#pragma once
#include "Plane.h"
#include "LightSource.h"
#include "Bullet.h"
#include "InputReader.h"

class Scene :
	public Object
{
public:
	Scene();
	~Scene();

	virtual void			Draw();
	virtual void			Update(float dtime);

	void					TreatInput(float dtime);
	void					ComputeMouseMovementAngle();

private:

	void					Init();
	void					ShootBullet();

	InputReader*			m_input;
	Vector2f				m_oldMousePos;
	Plane*					m_plane;
	LightSource*			m_lightSource;
	std::vector<Bullet*>	m_bullets;
	float					m_shootCooldown;

};

extern Scene* g_scene;

