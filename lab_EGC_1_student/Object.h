#pragma once
#include "shader.h"
#include "texture.h"
#include "transform.h"
#include "mesh.h"

class Object
{

public:
	Object();
	~Object();

	virtual void		Draw();
	virtual void		Update(float dtime);

	void				SetPosition(Vector3f& pos) { m_transform->SetPos(pos); }
	Vector3f*			GetPosition() { return m_transform->GetPos(); }

	Vector3f*			GetRot() { return m_transform->GetRot(); }
	void				SetRot(Vector3f& rot) { m_transform->SetRot(rot); }

	Vector3f*			GetScale() { return m_transform->GetScale(); }
	void				SetScale(Vector3f& scale) { m_transform->SetScale(scale); }

protected:

	Mesh*					m_mesh;
	Shader*					m_shader;
	Texture*				m_texture;
	Transform*				m_transform;

};

