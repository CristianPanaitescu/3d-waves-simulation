#pragma once

#include "Utils.h"

class Camera
{
public:
	Camera(const Vector3f& pos, float fov, float aspect, float zNear, float zFar);
	~Camera();
	
	glm::mat4		GetViewMatrix() const;
	glm::mat4		GetProjectionMatrix() const;

	Vector3f		GetForward() { return forward; }

	Vector3f		GetPosition() { return pos; };
	void			SetPosition(Vector3f pos) { this->pos = pos; }

	void			MoveForward(float amt);
	void			MoveRight(float amt);

	void			RotateX(float angle);
	void			RotateY(float angle);
protected:
private:
	glm::mat4 projection;
	Vector3f pos;
	Vector3f forward;
	Vector3f up;
};

extern Camera* g_camera;

