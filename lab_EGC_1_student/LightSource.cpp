#include "LightSource.h"


LightSource::LightSource()
{
	std::vector<Vertex> vertices;
	std::vector<uint> indices;

	InitVertices(vertices);
	InitIndices(indices);

	m_mesh = new Mesh(vertices, vertices.size(), indices, indices.size(), false);
	m_shader = new Shader("./res/basicShader", 2);
	m_texture = new Texture("./res/salam.jpg");
	m_transform = new Transform();
}


LightSource::~LightSource()
{
}

void LightSource::InitVertices(std::vector<Vertex>& vertices)
{
	int rings = 12;
	int sectors = 24;
	float radius = 0.2f;
	float const R = 1.f / (float)(rings - 1);
	float const S = 1.f / (float)(sectors - 1);
	int r, s;

	for (r = 0; r < rings; r++) 
		for (s = 0; s < sectors; s++) {
			float const y = (const float)sin(-(M_PI / 2) + M_PI * r * R);
			float const x = (const float)cos(2 * M_PI * s * S) * (const float)sin(M_PI * r * R);
			float const z = (const float)sin(2 * M_PI * s * S) *(const float)sin(M_PI * r * R);

			float ringsCoord = 0.f;
			if ( r > 8)
				ringsCoord = 0;
			else if ( r < 3)
				ringsCoord = 1;
			else
				ringsCoord = 1 - r * R;

			vertices.push_back(Vertex(
				Vector3f(x * radius, y * radius, z * radius), 
				Vector2f(s * S * 2, ringsCoord),
				Vector3f(x, y, z)));
		}
}

void LightSource::InitIndices(std::vector<uint>& indices)
{
	int rings = 12;
	int sectors = 24;
	for (int r = 0; r < rings - 1; r++)
		for (int s = 0; s < sectors - 1; s++) {
			
			indices.push_back((r + 1) * sectors + s);
			indices.push_back((r + 1) * sectors + (s + 1));
			indices.push_back(r * sectors + (s + 1));
			indices.push_back(r * sectors + s);
		}
}

