#pragma once
#include <iostream>
#include <SDL2/SDL.h>
#include "display.h"
#include "mesh.h"
#include "shader.h"
#include "texture.h"
#include "transform.h"
#include "camera.h"
#include "InputReader.h"

static const int DISPLAY_WIDTH = 1366;
static const int DISPLAY_HEIGHT = 700;

class App
{
public:
	App();
	~App();

	void			Run();

private:
	bool			m_isRunning;


};

extern App* g_app;
