#include "Scene.h"
#include "App.h"

Scene* g_scene = nullptr;

Scene::Scene()
{
	assert(g_scene == nullptr);
	g_scene = this;

	m_input = new InputReader();

	Init();
}


Scene::~Scene()
{
}

void Scene::Init()
{
	Transform* transform = new Transform();

	m_plane = new Plane();
	m_lightSource = new LightSource();
	m_lightSource->SetPosition(Vector3f(0.f, 3.f, 0.f));
	m_shootCooldown = 0;

	m_oldMousePos = Vector2f(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2);
}

void Scene::TreatInput(float dtime)
{
	if (m_input->IsKeyPressed('a'))
		g_camera->MoveRight(dtime * 0.01f);
	if (m_input->IsKeyPressed('d'))
		g_camera->MoveRight(-dtime * 0.01f);
	if (m_input->IsKeyPressed('w'))
		g_camera->MoveForward(dtime * 0.01f);
	if (m_input->IsKeyPressed('s'))
		g_camera->MoveForward(-dtime * 0.01f);
	if (m_input->IsKeyPressed('r'))
		g_camera->SetPosition(Vector3f(4.0f, 4.0f, 0.0f));
	
	if (m_input->IsKeyPressed('j'))
		m_lightSource->GetPosition()->x -= dtime * 0.005f;
	if (m_input->IsKeyPressed('l'))
		m_lightSource->GetPosition()->x += dtime * 0.005f;

	if (m_input->IsKeyPressed('i'))
		m_lightSource->GetPosition()->z -= dtime * 0.005f;
	if (m_input->IsKeyPressed('k'))
		m_lightSource->GetPosition()->z += dtime * 0.005f;

	if (m_input->IsKeyPressed('u'))
		m_lightSource->GetPosition()->y -= dtime * 0.005f;
	if (m_input->IsKeyPressed('o'))
		m_lightSource->GetPosition()->y += dtime * 0.005f;

	if (g_input->IsMousePressed())
		ShootBullet();
}

void Scene::ShootBullet()
{
	if (m_shootCooldown > 0)
		return;

	m_bullets.push_back(new Bullet(g_camera->GetPosition(), g_camera->GetForward()));
	m_shootCooldown = 1000;
	
}

void Scene::ComputeMouseMovementAngle()
{
	Vector2f newPos = m_input->GetNewMousePosition();

	Vector2f angle = Vector2f(0.2 * float(m_oldMousePos.y - newPos.y), 0.2 * float(m_oldMousePos.x - newPos.x));

	if (angle.x != 0)
		g_camera->RotateX(-angle.x);
	if (angle.y != 0)
		g_camera->RotateY(angle.y);
}

void Scene::Update(float dtime)
{
	if (m_shootCooldown > 0)
		m_shootCooldown -= dtime;
	else
		m_shootCooldown = 0;

	TreatInput(dtime);
	m_plane->UpdateShaderLight(*m_lightSource->GetPosition());
	m_plane->UpdateShaderEye(g_camera->GetPosition());
	m_plane->Update(dtime);
	m_lightSource->Update(dtime);
	for (uint i = 0; i < m_bullets.size(); i++)
	{
		m_bullets[i]->Update(dtime);
		if (m_bullets[i]->IsAlive())
		{
			if (m_bullets[i]->HitTheWatter())
			{
				m_plane->GenerateRadialWave(*m_bullets[i]->GetPosition());
				m_bullets.erase(m_bullets.begin() + i);
				i--;
			}
		}
		else
		{
			m_bullets.erase(m_bullets.begin() + i);
			i--;
		}
	
	}
}

void Scene::Draw()
{
	m_plane->Draw();
	m_lightSource->Draw();
	for (uint i = 0; i < m_bullets.size(); i++)
	{
		m_bullets[i]->Draw();
	}
}

