#pragma once
#include "Object.h"
class Bullet :
	public Object
{
public:
	Bullet(Vector3f position, Vector3f velocity);
	~Bullet();

	virtual void	Update(float dtime) override;
	bool			IsAlive() { return m_lifeTime > 0.f; }
	bool			HitTheWatter() { return GetPosition()->y <= 0; }

private:

	void			InitVertices(std::vector<Vertex>& vertices);
	void			InitIndices(std::vector<uint>& indices);

	Vector3f		m_velocity;
	float			m_speed;
	float			m_lifeTime;
};

