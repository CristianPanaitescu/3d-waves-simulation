#pragma once
#include "Utils.h"

class Display
{
public:
	Display(int width, int height, const std::string& title);

	virtual ~Display();

	void Clear(float r, float g, float b, float a);
	void SwapBuffers();
	void SetMouseAtPosition(int x, int y);

protected:
private:

	SDL_Window* m_window;
	SDL_GLContext m_glContext;
};

