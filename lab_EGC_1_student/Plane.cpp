#include "Plane.h"

#define VERTICES_LINE 200
#define VERTICES_COL 200

Plane::Plane()
{
	std::vector<Vertex> vertices;
	std::vector<uint> indices;

	m_sz = 0.04f;

	InitVertices(vertices);
	InitIndices(indices);

	m_mesh = new Mesh(vertices, vertices.size(), indices, indices.size(), true);
	m_shader = new Shader("./res/planeShader", 3);
	m_texture = new Texture("./res/water.jpg"); 
	m_transform = new Transform();
	
	m_waves = new Waves();
	m_waves->addWave(0.04f, 0.4f, glm::normalize(Vector2f(1, 0)), 1);
	///*m_waves->addWave(0.12f, 0.2f, glm::normalize(Vector2f(-1, 0)), 1);
	//m_waves->addWave(0.12f, 0.2f, glm::normalize(Vector2f(0, 1)), 1);
	//m_waves->addWave(0.12f, 0.2f, glm::normalize(Vector2f(0, -1)), 1);*/
	//m_waves->addWave(0.04f, 0.5f, Vector2f(0, 0), 0, Vector2f(2, 2.5f));
	//m_waves->addWave(0.02f, 0.34f, Vector2f(0, 0), 0, Vector2f(2, 1.5f));
}

void Plane::InitVertices(std::vector<Vertex>& vertices)
{
	float step = 0.02f;
	float partX = 0.0f, partZ = 0.0f;
	int k;
	for (int i = 0; i < VERTICES_LINE; i++)
	{
		k = i % 50;
		partZ = k * step;
		for (int j = 0; j < VERTICES_COL; j++)
		{
			k = j % 50;
			partX = k * step;
			vertices.push_back(Vertex(Vector3f(i * m_sz, 0, j * m_sz), glm::vec2(partZ, partX), Vector3f(0, 1, 0)));
			vertices.push_back(Vertex(Vector3f(i * m_sz, 0, (j + 1) * m_sz), glm::vec2(partZ, partX + step), Vector3f(0, 1, 0)));
			vertices.push_back(Vertex(Vector3f((i + 1) * m_sz, 0, (j + 1) * m_sz), glm::vec2(partZ + step, partX + step), Vector3f(0, 1, 0)));
			vertices.push_back(Vertex(Vector3f((i + 1) * m_sz, 0, j * m_sz), glm::vec2(partZ + step, partX), Vector3f(0, 1, 0)));
		}
	}
}

void Plane::InitIndices(std::vector<uint>& indices)
{
	for (int i = 0; i < VERTICES_LINE; i++)
	{
		for (int j = 0; j < VERTICES_COL; j++)
		{
			const uint noOfVertices = 4;
			const uint index = (i * VERTICES_COL + j) * noOfVertices;

			indices.push_back(index + 0);
			indices.push_back(index + 1);
			indices.push_back(index + 2);
			indices.push_back(index + 0);
			indices.push_back(index + 2);
			indices.push_back(index + 3);
		}
	}
}


Plane::~Plane()
{
}

void Plane::Update(float dtime)
{
	for (int i = 0; i < (int)m_waves->wavesNo; i++)
	{
		m_waves->timers[i] += dtime;
		float lt = m_waves->lifetimes[i];
		if (lt > 0)
		{
			float timePassed = m_waves->lifetimes[i] - dtime;
			m_waves->amps[i] -= (timePassed / lt) * m_waves->amps[i] * dtime / 2000;
			m_waves->maxRanges[i] += dtime * m_waves->wavesSpeed;
		}
		if (m_waves->amps[i] < 0.0001)
		{
			m_waves->removeWave(i--);
		}
			
	}
}

void Plane::Draw()
{
	m_shader->Bind();
	m_texture->Bind();
	m_shader->Update(m_transform);

	m_shader->SetAttribute("waves_no", m_waves->wavesNo);
	m_shader->SetAttribute("size", m_sz);
	m_shader->SetAttribute("S", 0.001f);
	m_shader->SetAttribute("material_kd", 0.8f);
	m_shader->SetAttribute("material_ks", 2.f);

	if (m_waves->wavesNo != 0)
	{
		m_shader->SetAttribute("amps", m_waves->amps);
		m_shader->SetAttribute("Ls", m_waves->Ls);
		m_shader->SetAttribute("dirs", m_waves->dirs);
		m_shader->SetAttribute("centers", m_waves->centers);
		m_shader->SetAttribute("dirTypes", m_waves->directionTypes);
		m_shader->SetAttribute("maxRanges", m_waves->maxRanges);
		m_shader->SetAttribute("T", m_waves->timers);
	}

	m_mesh->Draw();
}

void Plane::UpdateShaderLight(Vector3f position)
{
	m_shader->UpdateLightPosition(position);
}

void Plane::UpdateShaderEye(Vector3f position)
{
	m_shader->UpdateEyePosition(position);
}

void Plane::GenerateRadialWave(Vector3f bulletPosition)
{
	if (m_waves->wavesNo < 10)
		m_waves->addWave(0.07f, 0.5f, Vector2f(0, 0), 0, Vector2f(bulletPosition.x * 2, bulletPosition.z * 2), 5.f);
}