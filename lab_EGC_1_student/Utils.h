#pragma once
#include "GL/glew.h"
#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"
#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>

typedef glm::vec4 Vector4f;
typedef glm::vec3 Vector3f;
typedef glm::vec2 Vector2f;

typedef unsigned int uint;
