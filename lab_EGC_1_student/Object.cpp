#include "Object.h"


Object::Object()
{
}


Object::~Object()
{
}

void Object::Draw()
{
	m_shader->Bind();
	m_texture->Bind();
	m_shader->Update(m_transform);
	m_mesh->Draw();
}

void Object::Update(float dtime)
{
}
