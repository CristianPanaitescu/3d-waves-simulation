#version 330 compatibility
layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

varying vec2 texCoord0[3];

out vec2 texCoords;

void main()
{	
  for(int i=0; i<3; i++)
  {
	texCoords = texCoord0[i];
    gl_Position = gl_in[i].gl_Position;
    EmitVertex();
  }
  EndPrimitive();
}  