#pragma once
#include "Utils.h"

class Texture
{
public:
	Texture(const std::string& fileName);

	void Bind();

	virtual ~Texture();
protected:
private:

	GLuint m_texture;
};

