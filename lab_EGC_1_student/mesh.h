#pragma once
#ifndef MESH_INCLUDED_H
#define MESH_INCLUDED_H

#include "Utils.h"
#include "obj_loader.h"

struct Vertex
{
public:
	Vertex(const Vector3f& pos, const glm::vec2& texCoord, const Vector3f& normal)
	{
		this->pos = pos;
		this->texCoord = texCoord;
		this->normal = normal;
	}

	Vector3f* GetPos() { return &pos; }
	glm::vec2* GetTexCoord() { return &texCoord; }
	Vector3f* GetNormal() { return &normal; }

private:
	Vector3f pos;
	glm::vec2 texCoord;
	Vector3f normal;
};

enum MeshBufferPositions
{
	POSITION_VB,
	TEXCOORD_VB,
	NORMAL_VB,
	INDEX_VB
};

class Mesh
{
public:
    Mesh(const std::string& fileName);
	Mesh(std::vector<Vertex> vertices, uint numVertices, std::vector<uint> indices, uint numIndices, bool useTriangles);

	void						Draw();

	virtual ~Mesh();

protected:
private:
	static const unsigned int	NUM_BUFFERS = 4;
		
    void						InitMesh(const IndexedModel& model);

	GLuint						m_vertexArrayObject;
	GLuint						m_vertexArrayBuffers[NUM_BUFFERS];
	unsigned int				m_numIndices;
	bool						m_useTriangles;
};

#endif
