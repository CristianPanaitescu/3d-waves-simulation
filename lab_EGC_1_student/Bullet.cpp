#include "Bullet.h"


Bullet::Bullet(Vector3f position, Vector3f velocity)
{
	std::vector<Vertex> vertices;
	std::vector<uint> indices;

	InitVertices(vertices);
	InitIndices(indices);

	m_mesh = new Mesh(vertices, vertices.size(), indices, indices.size(), false);
	m_shader = new Shader("./res/basicShader", 2);
	m_texture = new Texture("./res/water.jpg");
	m_transform = new Transform();

	SetPosition(position);
	m_velocity = velocity;
	m_speed = 0.003f;
	m_lifeTime = 100000.f;
}


Bullet::~Bullet()
{
}

void Bullet::Update(float dtime)
{
	m_lifeTime -= dtime;
	SetPosition(*GetPosition() + m_velocity * m_speed * dtime);
}

void Bullet::InitVertices(std::vector<Vertex>& vertices)
{
	int rings = 12;
	int sectors = 24;
	float radius = 0.02f;
	float const R = 1.f / (float)(rings - 1);
	float const S = 1.f / (float)(sectors - 1);
	int r, s;

	for (r = 0; r < rings; r++)
	for (s = 0; s < sectors; s++) {
		float const y = (const float)sin(-(M_PI / 2) + M_PI * r * R);
		float const x = (const float)cos(2 * M_PI * s * S) * (const float)sin(M_PI * r * R);
		float const z = (const float)sin(2 * M_PI * s * S) *(const float)sin(M_PI * r * R);

		float ringsCoord = 0.f;
		if (r > 8)
			ringsCoord = 0;
		else if (r < 3)
			ringsCoord = 1;
		else
			ringsCoord = 1 - r * R;

		vertices.push_back(Vertex(
			Vector3f(x * radius, y * radius, z * radius),
			Vector2f(s * S * 2, r * R),
			Vector3f(x, y, z)));
	}
}

void Bullet::InitIndices(std::vector<uint>& indices)
{
	int rings = 12;
	int sectors = 24;
	for (int r = 0; r < rings - 1; r++)
	for (int s = 0; s < sectors - 1; s++) {

		indices.push_back((r + 1) * sectors + s);
		indices.push_back((r + 1) * sectors + (s + 1));
		indices.push_back(r * sectors + (s + 1));
		indices.push_back(r * sectors + s);
	}
}