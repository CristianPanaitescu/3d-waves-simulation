#version 330

attribute vec3 position;
attribute vec2 texCoord;
attribute vec3 normal;

varying vec2 texCoord0;
varying vec3 normal0;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

uniform vec3 light_position;
uniform vec3 eye_position;

uniform mat4 Normal;

void main()
{
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(position, 1.0);
	texCoord0 = texCoord;
}
