#pragma once
#include "Utils.h"

#define KEYS_NO 255

class InputReader
{
public:
	InputReader();
	~InputReader();

	void		ReadKey(SDL_Event& event);

	bool		IsKeyPressed(char key) { return keys[key]; }
	bool		IsMousePressed();
	Vector2f	GetNewMousePosition();

private:
	
	void		RefreshKeys();

	bool		keys[KEYS_NO];
	bool		m_mousePressed;
};

extern InputReader* g_input;

