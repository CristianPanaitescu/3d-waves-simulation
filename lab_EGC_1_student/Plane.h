#pragma once
#include "Object.h"

struct Waves
{
	float						wavesNo = 0;
	float						wavesSpeed = 0.001f;
	std::vector<float>			amps;
	std::vector<float>			Ls;
	std::vector<Vector2f>		dirs;
	std::vector<Vector2f>		centers;
	std::vector<float>			directionTypes;
	std::vector<float>			lifetimes;
	std::vector<float>			maxRanges;
	std::vector<float>			timers;

	//directiontype 0 -> radial
	//directiontype 1 -> directional
	void addWave(float amp, float L, Vector2f dir, float directionType = 0, Vector2f center = Vector2f(2.f, 0.f), float lifetime = 0, float maxRange = 0)
	{
		amps.push_back(amp);
		Ls.push_back(L);
		dirs.push_back(dir);
		directionTypes.push_back(directionType);
		centers.push_back(center);
		lifetimes.push_back(lifetime * 1000);
		maxRanges.push_back(maxRange);
		timers.push_back(-150.f);
		wavesNo++;
	}

	void removeWave(int index)
	{
		amps.erase(amps.begin() + index);
		Ls.erase(Ls.begin() + index);
		dirs.erase(dirs.begin() + index);
		directionTypes.erase(directionTypes.begin() + index);
		centers.erase(centers.begin() + index);
		lifetimes.erase(lifetimes.begin() + index);
		maxRanges.erase(maxRanges.begin() + index);
		timers.erase(timers.begin() + index);
		wavesNo--;
	}

};

class Plane :
	public Object
{
public:
	Plane();
	~Plane();

	virtual void		Draw();
	virtual void		Update(float dtime) override;
	void				UpdateShaderLight(Vector3f position);
	void				UpdateShaderEye(Vector3f position);

	void				GenerateRadialWave(Vector3f bulletPosition);

private:

	void				InitVertices(std::vector<Vertex>& vertices);
	void				InitIndices(std::vector<uint>& indices);

	Waves*				m_waves;
	float				m_sz;
};

